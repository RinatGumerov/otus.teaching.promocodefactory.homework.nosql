﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class ReferenceBookGateway : IReferenceBookGateway
    {
        private readonly HttpClient _httpClient;

        public ReferenceBookGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<Preference>> GetPreferencesAsync()
        {
            var preferencesMessage = await _httpClient.GetAsync("api/v1/preferences");

            preferencesMessage.EnsureSuccessStatusCode();

            var preferences = JsonConvert.DeserializeObject<IEnumerable<Preference>>(
                await preferencesMessage.Content.ReadAsStringAsync());

            return preferences;
        }
    }
}
