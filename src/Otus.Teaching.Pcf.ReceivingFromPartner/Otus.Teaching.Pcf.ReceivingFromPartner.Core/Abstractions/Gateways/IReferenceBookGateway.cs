﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways
{
    public interface IReferenceBookGateway
    {
        Task<IEnumerable<Preference>> GetPreferencesAsync();
    }
}