﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways
{
    public interface IReferenceBookGateway
    {
        Task<IEnumerable<Preference>> GetPreferencesAsync();
    }
}