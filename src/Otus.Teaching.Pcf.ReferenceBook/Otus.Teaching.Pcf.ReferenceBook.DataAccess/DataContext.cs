﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.ReferenceBook.Core.Domain;
using Otus.Teaching.Pcf.ReferenceBook.DataAccess.Data;

namespace Otus.Teaching.Pcf.ReferenceBook.DataAccess
{
    public class DataContext
        : DbContext
    {

        public DbSet<Preference> Preferences { get; set; }

        public DataContext()
        {
            
        }
        
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }
}