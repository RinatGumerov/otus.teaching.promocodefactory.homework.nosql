﻿using System;

namespace Otus.Teaching.Pcf.ReferenceBook.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}