﻿using System;
using Otus.Teaching.Pcf.ReferenceBook.Core.Domain;

namespace Otus.Teaching.Pcf.ReferenceBook.Core.Domain
{
    public class Preference
        :BaseEntity
    {
        public string Name { get; set; }
    }
}