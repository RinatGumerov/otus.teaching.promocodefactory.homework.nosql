﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.ReferenceBook.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.ReferenceBook.Core.Domain;
using Otus.Teaching.Pcf.ReferenceBook.WebHost.Extensions;
using Otus.Teaching.Pcf.ReferenceBook.WebHost.Models;

namespace Otus.Teaching.Pcf.ReferenceBook.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private const string preferenceCacheKey = "PreferencesControllerCacheKey";
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IDistributedCache _distributedCache;

        public PreferencesController(IRepository<Preference> preferencesRepository, IDistributedCache distributedCache)
        {
            _preferencesRepository = preferencesRepository;
            _distributedCache = distributedCache;
        }
        
        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<Preference>>> GetPreferencesAsync()
        {
            if (_distributedCache.TryGetValue(preferenceCacheKey, out IEnumerable<Preference> preferences))
            {
                return Ok(preferences);
            }
            else
            {
                preferences = await _preferencesRepository.GetAllAsync();
                var cacheEntryOptions = new DistributedCacheEntryOptions()
                        .SetSlidingExpiration(TimeSpan.FromSeconds(60))
                        .SetAbsoluteExpiration(TimeSpan.FromSeconds(3600));
                await _distributedCache.SetAsync(preferenceCacheKey, preferences, cacheEntryOptions);
                return Ok(preferences);
            }
        }
    }
}