﻿using System;

namespace Otus.Teaching.Pcf.ReferenceBook.WebHost.Models
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
    }
}